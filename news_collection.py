from pymongo import MongoClient

limit = 200000
dict_taxonomy = {}
collection = MongoClient('104.155.210.134')["client_text"]["news_data"]
collection2 = MongoClient('104.155.210.134')["tagging"]["headlines"]

count = collection.count()

cursor = collection.find({},{"Highlight":1,"taxonomy":1,"_id":0})
'''
for cur in cursor:
	keys = dict_taxonomy.keys()
	taxonomy = cur.get('taxonomy')
	if taxonomy is None:
		continue
	for item in taxonomy:
		if type(item) == list:
			item = item[0]
		if item in keys:
			continue
		else:
			dict_taxonomy[item] = 1

print dict_taxonomy
keys = dict_taxonomy.keys()
length = len(keys)
'''

length = 158
total = 0
threshold = limit/length
for cur in cursor:
	keys = dict_taxonomy.keys()
	values = dict_taxonomy.values()
	for value in values:
		total = total + value
	if total == limit:
		break
	key_length = len(keys)
	taxonomy = cur.get('taxonomy')
	if taxonomy is None:
		continue
	for item in taxonomy:
		if type(item) == list:
			item = item[0]
		if item not in keys:
			dict_taxonomy[item] = 1
			collection2.insert({"heading":cur['Highlight'],"taxonomy":item})
			break
		elif ((item in keys) and (dict_taxonomy[item] < threshold)):
			dict_taxonomy[item] += 1
			collection2.insert({"heading":cur['Highlight'],"taxonomy":item})
			break
		elif dict_taxonomy[item] >= threshold:
			continue 

print "done"
