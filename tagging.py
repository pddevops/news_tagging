# -*- coding: utf-8 -*-
import tornado.web
import tornado.ioloop
import json
from pymongo import MongoClient
import datetime

max_count = 2

collection_news = MongoClient('104.155.210.134')["tagging"]["headlines"]
count_news = collection_news.count()
        
collection_tag = MongoClient('104.155.210.134')["tagging"]["taginfo"]

collection_user = MongoClient('104.155.210.134')["tagging"]["userinfo"]

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        self.write("Hello, World")

class TaggingHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        headlines = []
        d = {}
        
        start_row = self.get_argument("start")
        limit = self.get_argument("limit")
        uid = self.get_argument("uid")
        restart = self.get_argument("restart")
        uid = str(uid)
        start_row = int(start_row)
        limit = int(limit)
        restart = str(restart)
        news_count = 0
        first_time_user = 1
        
        #Database containing userinfo
        query = collection_user.find_one({"user_id":uid},{'last_visited':1})
        if query is not None:
            if restart == "yes":
                first_time_user = 0
                start_row = 0
                last_visited = 0
            elif restart == "no":
                first_time_user = 0
                last_visited = query['last_visited']
                start_row = last_visited
        
        if first_time_user == 1:
            last_visited = start_row+limit
            tag_count = 0
            now = datetime.datetime.now()
            date = now.strftime("%d-%m-%Y")
            count = {}
            count[date] = 0
            collection_user.insert({"user_id":uid,"last_visited":last_visited,"tag_count":tag_count,"datewise_count":count})
        else:
            last_visited = last_visited+limit
            collection_user.update({"user_id":uid},{'$set':{"last_visited":last_visited}})

        #Database containing news headings
        cursor_news = collection_news.find({"flag":0},{"heading":1,"_id":0}).skip(start_row)
        #Traversing the database to display news
        for cur_news in cursor_news:
            if (news_count == limit):
                break
            user_flag = 0
            query = collection_tag.find_one({"heading":cur_news['heading']},{'userinfo':1,'sentiment_count':1})
            #Checking whether the heading has been tagged by any user before or not
            if query is not None:
                userinfo = query['userinfo']
                sentiment_count = query['sentiment_count']
                #Checking whether the user has tagged that sentence or not
                for user in userinfo:     
                    if uid == user['uid']:
                        user_flag = 1
                        break
                    else:
                        user_flag = 0
                #Checking the heading for tag count < max_count
                if((sentiment_count < max_count) and (user_flag == 0)):
                    headlines.append(cur_news['heading'])
                    news_count+=1
            else:
                headlines.append(cur_news['heading'])
                news_count+=1
        
        print "Done"
        d["data"] = headlines
        
        #checking whether the data is empty or not
        if d["data"] == []:
            d["next"] = None
        else:
            start_row = start_row+limit
        
            if start_row >= count_news:
                start_row = 0
                d["next"] = "http://utilities.paralleldots.com/tagging?start=%d&restart=yes&limit=%d&uid=%s"%(start_row,limit,uid)
            else:                
                d["next"] = "http://utilities.paralleldots.com/tagging?start=%d&restart=no&limit=%d&uid=%s"%(start_row,limit,uid)
        self.write(d)
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        #Load the JSON data
        news_data = json.loads(self.request.body)
        sentiment_flag = 0
        userdata = {}
        print news_data
        #Traversing the data send from the browser to post to database
        for news in news_data:
            news_heading = news.get('heading',None)
            #news_heading = news_heading.decode('string_escape').replace("\\","")
            user_id = news.get('uid',None)
            query = collection_user.find_one({"user_id":user_id},{"tag_count":1,"datewise_count":1})
            if query is not None:
                tag_count = query['tag_count']
                tag_count+=1
                count = {}
                count = query['datewise_count']
                now = datetime.datetime.now()
                date = now.strftime("%d-%m-%Y")
                if date in count.keys():
                    count[date] += 1
                else:
                    count[date] = 1
                collection_user.update({'user_id':user_id},{'$set':{'tag_count':tag_count,"datewise_count":count}})
            query = collection_tag.find_one({"heading":news_heading,"flag":0},{"sentiment":1,"sentiment_count":1,"userinfo":1})
            if query is not None:   
                data_dict = {}
                sentiment = news.get('sentiment',None)
                sentiment_dict = query['sentiment']
                count = query['sentiment_count']
                userinfo = query['userinfo']
                data_dict['sentiment'] = sentiment
                data_dict['classifier'] = news.get('classifier',None)
                data_dict['adult'] = news.get('adult',None)
                data_dict['abusive'] = news.get('abusive',None)
                data_dict['TargetedSentiment'] = news.get('targeted',None)
                data_dict['category'] = news.get('category',None) 
                data_dict['emotions'] = news.get('emotion',None)
                userdata['uid'] = news['uid']
                userdata['input'] = data_dict
                userdata['timestamp'] = datetime.datetime.now()
                keys = sentiment_dict.keys()
                userinfo.append(userdata)
                #Checking the sentiment for ambiguity
                if ((sentiment in keys) and (sentiment_dict[sentiment] < max_count)):
                    sentiment_dict[sentiment]+=1
                    if sentiment_dict[sentiment] == max_count:
                        count=max_count
                        collection_news.update({"heading":news_heading},{'$set':{"flag":1}})
                    collection_tag.update({"heading":news_heading},{'$set':{"sentiment_count":count,"userinfo":userinfo,"sentiment":sentiment_dict}})
                elif sentiment not in keys:
                    if sentiment is not None:
                        sentiment_dict[sentiment] = 1
                    collection_tag.update({"heading":news_heading},{'$set':{"sentiment_count":count,"userinfo":userinfo,"sentiment":sentiment_dict}})
            #If heading is not present in database
            else:
                sentiment_dict = {}
                data_dict = {}
                userinfo = []
                sentiment = news.get('sentiment',None)
                if sentiment is not None:
                    sentiment_dict[sentiment] = 1
                data_dict['sentiment'] = sentiment
                data_dict['classifier'] = news.get('classifier',None)
                data_dict['adult'] = news.get('adult',None)
                data_dict['abusive'] = news.get('abusive',None)
                data_dict['TargetedSentiment'] = news.get('targeted',None)
                data_dict['category'] = news.get('category',None) 
                data_dict['emotions'] = news.get('emotion',None)
                userdata['uid'] = news.get('uid',None)
                userdata['input'] = data_dict
                userdata['timestamp'] = datetime.datetime.now()
                userinfo.append(userdata)
                collection_tag.insert({"heading":news_heading,"sentiment_count":1,"userinfo":userinfo,"sentiment":sentiment_dict})
        self.write("Saved Successfully")
        return

class UserInfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        data = {}
        d = {}
        user_id = self.get_argument("uid")
        user_id = str(user_id)
        cursor_user = collection_user.find({'user_id':user_id},{"user_id":1,"tag_count":1,"_id":0})
        
        for cur_user in cursor_user:
            d = {}
            d['user_id'] = cur_user['user_id']
            d['tag_count'] = cur_user['tag_count']

        data['info'] = d
        self.write(data)

class TaggerInfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        now = datetime.datetime.now()
        date = now.strftime("%d-%m-%Y")
        d = {}
        data_list = []
        cursor = collection_user.find()
        for cur in cursor:
            data = {}
            data["user_id"] = cur["user_id"]
            data["total"] = cur["tag_count"]
            datewise_count = {}
            datewise_count = cur["datewise_count"]
            if date in datewise_count.keys():
                data["today"] = datewise_count[date]
            else:
                data["today"] = 0
            data_list.append(data)
        d["data"] = data_list
	self.write(d)

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),(r"/tagging",TaggingHandler),(r"/userinfo",UserInfoHandler),(r"/taggerinfo",TaggerInfoHandler)
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8886)
    tornado.ioloop.IOLoop.current().start()
